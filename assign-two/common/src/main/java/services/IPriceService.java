package services;

import enitity.Car;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IPriceService extends Remote {

    double computeSellingPrice(Car c) throws RemoteException;

}
