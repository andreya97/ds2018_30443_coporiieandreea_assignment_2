package service_implem;

import enitity.Car;
import services.IPriceService;

public class PriceService implements IPriceService {
    public double computeSellingPrice(Car c) {
        if (2018 - c.getYear() < 7)
            return c.getPrice() - (c.getPrice() / 7) * (2018 - c.getYear());
        else
            return 0;
    }
}
