package server;

import service_implem.PriceService;
import service_implem.TaxService;
import services.IPriceService;
import services.ITaxService;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ServerStart {

    public static void main(String[] args) {
        ITaxService taxService = new TaxService();
        IPriceService priceService = new PriceService();
        try {
            ITaxService taxServiceStub = (ITaxService) UnicastRemoteObject.exportObject((ITaxService) taxService, 0);
            IPriceService priceServiceStub = (IPriceService) UnicastRemoteObject.exportObject((IPriceService) priceService, 0);

            Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
            registry.rebind("ITaxService", taxServiceStub);
            registry.rebind("IPriceService", priceServiceStub);

            System.out.println("Server started.");
        } catch (RemoteException e) {
            System.out.println("Server no can do: " + e.getMessage());
        }
    }
}
