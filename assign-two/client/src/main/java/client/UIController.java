package client;

import enitity.Car;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import services.IPriceService;
import services.ITaxService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class UIController {

    @FXML
    private AnchorPane pane;

    @FXML
    private Label titleLabel;

    @FXML
    private Label yearLabel;

    @FXML
    private Label engineLabel;

    @FXML
    private Label priceLabel;

    @FXML
    private TextField engineTF;

    @FXML
    private TextField yearTF;

    @FXML
    private TextField priceTF;

    @FXML
    private Button taxButton;

    @FXML
    private Button priceButton;

    @FXML
    private TextArea responseArea;

    @FXML
    void doComputeSellingPrice(ActionEvent event) {
        try {
            Registry registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
            IPriceService priceService = (IPriceService) registry.lookup("IPriceService");

            Car c = createCar();
            if (c != null)
                responseArea.setText("Selling value is " + priceService.computeSellingPrice(c));
        } catch (RemoteException | NotBoundException e) {
            responseArea.setText("Selling problems is " + e.getMessage());
        }
    }

    @FXML
    void doComputeTax(ActionEvent event) {
        try {
            Registry registry = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
            ITaxService taxService = (ITaxService) registry.lookup("ITaxService");

            Car c = createCar();
            if (c != null)
                responseArea.setText("Tax value: " + taxService.computeTax(c));
        }catch (RemoteException | NotBoundException e) {
            responseArea.setText("Tax problems: " + e.getMessage());
        }
    }

    private Car createCar() {

        try {
            int year = Integer.parseInt(yearTF.getText());
            int engine = Integer.parseInt(engineTF.getText());
            double price = Double.parseDouble(priceTF.getText());

            return new Car(year, engine, price);
        } catch (NumberFormatException e) {
            responseArea.setText("All the inputs must be valid numbers.");
            return  null;
        }
    }

}
